(function($){
  Drupal.ajax.prototype.update = function() {
    var ajax = this;

    // Do not perform another ajax command if one is already in progress.
    if (ajax.ajaxing) {
      return false;
    }

    // Update url
    ajax.options.url = '/?q=admin/reports/miniprofiler/' + Drupal.settings.miniprofiler.hashes.join('+') + '/callback';

    try {
      $.ajax(ajax.options);
    }
    catch (err) {
      alert('An error occurred while attempting to process ' + ajax.options.url);
      return false;
    }

    return false;
  };

  /**
   * Define a custom ajax action.
   */
  Drupal.ajax['miniprofile'] = new Drupal.ajax(null, $(document.body), {
    url: 'admin/reports/miniprofiler/hash/callback',
    settings: {
      keypress: false,
      prevent: false,
    },
    event: 'onload'
  });

  /**
   * Define a point to trigger our custom actions. e.g. on page load.
   */
  $(document).ready(function() {
    Drupal.ajax['miniprofile'].update();
  });

  /**
   * And let any following ajax calls also do the update
   */
  $.fn.miniprofileUpdate = function(hash) {
    Drupal.settings.miniprofiler.hashes.push(hash);
    Drupal.ajax['miniprofile'].update();
  };

})(jQuery);
