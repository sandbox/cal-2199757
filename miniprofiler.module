<?php

/**
 * Implements hook_permission().
 */
function miniprofiler_permission() {
  return array(
    'access miniprofiler' => array(
      'title' => t('Access MiniProfiler'),
    )
  );
}

/**
 * Implements hook_menu().
 */
function miniprofiler_menu() {
  $items['admin/reports/miniprofiler/%miniprofiler_hash/callback'] = array(
    'access arguments' => array('access miniprofiler'),
    'delivery callback' => 'ajax_deliver',
    'page arguments' => array(3),
    'page callback' => '_miniprofiler_callback',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_init().
 */
function miniprofiler_init() {
  if (!_miniprofiler_enabled()) { return; }
  if (arg(0) == 'system' && arg(1) == 'ajax') { return; }

  drupal_add_library('system', 'drupal.ajax');
  drupal_add_js(drupal_get_path('module', 'miniprofiler') .'/miniprofiler.js');

  $settings = array(
    'hashes' => array(
      _miniprofiler_get_hash(),
    ),
  );
  drupal_add_js(array('miniprofiler' => $settings), 'setting');
}

/**
 * Implements hook_exit().
 */
function miniprofiler_exit() {
  if (!_miniprofiler_enabled()) { return; }

  $timer = (int) timer_read('page');

  cache_set('miniprofiler-'. _miniprofiler_get_hash(), $timer, 'cache', CACHE_TEMPORARY);
}

/**
 * Implements hook_page_alter().
 */
function miniprofiler_page_alter(&$page) {
  if (!_miniprofiler_enabled()) { return; }

  if (isset($page['page_top']['toolbar'])) {
    $page['page_top']['toolbar']['#pre_render'][] = 'miniprofiler_toolbar_pre_render';
  }
}

/**
 * Implements hook_ajax_render_alter().
 */
function miniprofiler_ajax_render_alter(&$commands) {
  if (!_miniprofiler_enabled()) { return; }

  $commands[] = ajax_command_invoke(NULL, 'miniprofileUpdate', array(_miniprofiler_get_hash()));
}

/**
 * Pre-render function for adding profiling data to the toolbar.
 */
function miniprofiler_toolbar_pre_render($toolbar) {
  $miniprofiler = array(
    'title' => '',
    'href' => 'admin/reports/miniprofiler',
    'attributes' => array(
      'id' => 'miniprofiler',
    ),
  );

  $toolbar['toolbar_user']['#links'] = array_reverse($toolbar['toolbar_user']['#links'], true);
  $toolbar['toolbar_user']['#links']['miniprofiler'] = $miniprofiler;
  $toolbar['toolbar_user']['#links'] = array_reverse($toolbar['toolbar_user']['#links'], true);

  return $toolbar;
}

function miniprofiler_hash_load($input) {
  // 'validate': strip all useless characters
  $input = preg_replace('/[^0-9a-zA-Z ]/', '', $input);
  $hashes = explode(' ', $input);
  return $hashes;
}

function _miniprofiler_callback($hashes) {
  $timer = 0;
  $details = array();
  foreach ($hashes as $hash) {
    $store = cache_get('miniprofiler-'. $hash);
    if ($store) {
      $timer += $store->data;
      $details[] = $store->data .'ms';
    }
  }
  $details = implode(' + ', $details);

  $miniprofiler = array(
    'title' => round($timer / 1000, 2) .'s',
    'href' => 'admin/reports/miniprofiler',
    'attributes' => array(
      'id' => 'miniprofiler',
      'title' => $details,
    ),
  );

  $html = l($miniprofiler['title'], $miniprofiler['href'], array('attributes' => $miniprofiler['attributes']));

  $commands = array();
  $commands[] = ajax_command_replace('#miniprofiler', $html);

  return array(
    '#type' => 'ajax',
    '#commands' => $commands,
  );
}

function _miniprofiler_get_hash() {
  $hash = &drupal_static(__FUNCTION__);
  if (!isset($hash)) {
    $length = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    $hash = $randomString;
  }
  return $hash;
}

function _miniprofiler_enabled() {
  $enabled = &drupal_static(__FUNCTION__);
  if (!isset($enabled)) {
    $enabled = user_access('access miniprofiler');

    // Don't do overlays
    if (function_exists("overlay_get_mode") && overlay_get_mode() == 'child') {
      $enabled = FALSE;
    }

    // Don't do our own ajax requests
    if (arg(0) == 'admin' && arg(1) == 'reports' && arg(2) == 'miniprofiler' && arg(4) == 'callback') {
      $enabled = FALSE;
    }
  }
  return $enabled;
}
